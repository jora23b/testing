<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->increments('id');

            $table->string('url', 255)->nullable();
            $table->unsignedTinyInteger('letter')->default(0);
            $table->boolean('top')->nullable();
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('rank')->nullable();
            $table->boolean('active');
            $table->boolean('desc')->nullable();
            $table->timestamps();

            $table->index('letter', 'index_letter');
        });

        Schema::create('game_translations', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('game_id')->nullable();
            $table
                ->foreign('game_id')
                ->references('id')
                ->on('games')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('language_id')->nullable();
            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->string('name', 200);
            $table->string('slug', 255);
            $table->text('description')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();

            $table->index('name', 'index_name');
            $table->index(['language_id', 'game_id']);
        });

        DB::statement('ALTER TABLE game_translations ADD FULLTEXT full(name)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_translations');
        Schema::dropIfExists('games');
    }
}
