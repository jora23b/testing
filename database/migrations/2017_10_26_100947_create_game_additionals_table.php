<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGameAdditionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_additionals', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_id');
            $table->foreign('game_id')->references('id')
                ->on('games')->onDelete('cascade')->onUpdate('cascade');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->boolean('active');
            $table->timestamps();
        });

        Schema::create('game_additional_translations', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('game_additional_id')->nullable();
            $table->foreign('game_additional_id')
                ->references('id')
                ->on('game_additionals')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->unsignedInteger('language_id')->nullable();
            $table
                ->foreign('language_id')
                ->references('id')
                ->on('languages')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->text('description')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_additional_translations');
        Schema::dropIfExists('game_additionals');
    }
}
