<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

switch (Request::server("SERVER_NAME")) {
    case 'kod-igra.ru':
        Config::set('app.fallback_locale', 'ru');
        break;
    case 'codici-giochi.it':
        Config::set('app.fallback_locale', 'it');
        break;
    default:
        Config::set('app.fallback_locale', 'ro');
}

\localizer\group(['middleware' => 'guest'], function () {
    Route::get('/', ['as' => 'home-page', 'uses' => 'GamesController@index']);
    Route::get('/games/{letter}', ['as' => 'game.letter', 'uses' => 'GamesController@index']);
    Route::get('/rss/{letter}', ['as' => 'rss', 'uses' => 'RssController@index']);
    Route::get('/sitemap', ['as' => 'sitemap', 'uses' => 'SitemapController@index']);
    Route::get('game/{game}', ['as' => 'game.view', 'uses' => 'GamesController@view']);
});

Route::get('/sitemap-images', ['as' => 'sitemap-images', 'uses' => 'SitemapController@images']);
