<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/26/17
 * Time: 15:30
 */

namespace App\Services;


use App\Game;

class GameService
{
    protected $model;

    public function __construct(Game $model)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model->where('active', 1);
    }

    public function getAllForSitemap()
    {
        $query = $this->getModel()
            ->select(['slug']);
        $query = $this->model->scopeTranslated($query);
        return $query->get();
    }

    public function getAllSitemapImages()
    {
        return $this->getModel()->where('image_file_name', '<>', '')->get();
    }

    public function getBySlug($slug)
    {
        $query = $this->getModel()
            ->select(['games.*', 'name', 'description', 'meta_title', 'meta_description', 'meta_keywords'])
            ->where('slug', $slug);
        $query = $this->model->scopeTranslated($query);
        return $query->first();
    }

    public function getByLetter($letter)
    {
        $query = $this->getModel()->where('letter', $letter)
            ->select(['games.id', 'meta_title', 'slug', 'meta_description']);
        $query = $this->model->scopeTranslated($query);

        return $query->get();
    }

    public function getPaginate($filters, $nrPerPage = 50)
    {
        $query = $this->getModel()
            ->select(['games.*', 'name'])
            //->orderBy('top', 'desc')
            //->orderBy('rank', 'asc')
            ->orderBy('name', 'ASC');
        if (isset($filters['letter']) && $filters['letter']) {
            $query->where('letter', $filters['letter']);
        }

        if (isset($filters['search']) && $filters['search']) {
            $query->whereRaw("MATCH(name) AGAINST('" . $filters['search'] . "')");
        }

        $query = $this->model->scopeTranslated($query);

        return $query->paginate($nrPerPage);
    }
}