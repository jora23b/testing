<?php

namespace App\Composers;

use App\Game;
use Illuminate\Routing\Route;
use Illuminate\Routing\Router;
use Illuminate\View\View;

class SeoTags
{
    use HandlesSeoPages;
    protected $locale;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var Route
     */
    protected $route;

    public function __construct(Router $router)
    {
        $this->router = $router;
        $this->route = $router->current();
        $this->locale = \localizer\locale();
    }

    public function compose(View $view)
    {
        if (!$route = $this->route) {
            return false;
        }
        $routeName = $route->getName();

        switch ($routeName) {
            case 'game.letter':
                return $this->letterMeta();
                break;
            case 'game.view':
                return is_a($game = $this->seoObject('game'), Game::class)
                    ? $this->gameMeta($game)
                    : $this->routeMeta('404');
                break;
            case 'home-page':
                return $this->homeMeta();

            default:
                return $this->routeMeta('404');
        }
    }

    protected function currentUrl()
    {
        return \URL::current();
    }
}
