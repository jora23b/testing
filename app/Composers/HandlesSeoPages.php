<?php

namespace App\Composers;

use App\Game;
use Artesaos\SEOTools\Contracts\SEOTools as SEOToolsContract;
use Artesaos\SEOTools\Traits\SEOTools;
use Illuminate\Database\Eloquent\Model;

trait HandlesSeoPages
{
    use SEOTools;

    /**
     * Retrieve the Seo Object by parameter name.
     *
     * @param string $type
     *
     * @return Model
     */
    protected function seoObject($type)
    {
        switch ($type) {
            case 'game':
                return $this->route->parameter($type);
        }
        return null;
    }

    /**
     * Set Car SEO tags.
     *
     * @param Game $car
     * @return SEOToolsContract
     */
    protected function gameMeta(Game $game)
    {
        $meta = $this->genericMeta($game);
        $meta->opengraph()->setUrl($this->currentUrl());
        if (file_exists(public_path($image = $game->image->url('medium')))) {
            $meta->opengraph()->addImage(url($image));
        }

        return $meta;
    }

    /**
     * Set generic SEO tags for eloquent object.
     *
     * @param $eloquent - a model having meta.
     * @return SEOToolsContract
     */
    protected function genericMeta($eloquent)
    {
        $descriptionKey = $this->descriptionKey();
        with($seo = $this->seo())
            ->setTitle(!empty($eloquent->meta_title) ? $eloquent->meta_title : $eloquent->name)
            ->setDescription(!empty($eloquent->meta_description) ? $eloquent->meta_description : $eloquent->$descriptionKey);
        $seo->metatags()->setKeywords(!empty($eloquent->meta_keywords) ? $eloquent->meta_keywords : $this->titleToKeywords($eloquent));
        $seo->opengraph()->addProperty('locale', $this->locale->locale());
        return $seo;
    }

    protected function descriptionKey()
    {
        return 'description';
    }

    /**
     * @param $eloquent
     * @return mixed
     */
    protected function titleToKeywords($eloquent)
    {
        return str_replace(' ', ', ', $eloquent->name);
    }

    public function routeMeta($name)
    {
        $this->seo()->setTitle('404' . trans("meta.{$name}.title"));
        $this->seo()->setDescription(trans("meta.{$name}.description"));
        $this->seo()->metatags()->setKeywords(trans("meta.{$name}.keywords"));
    }

    public function homeMeta()
    {
        $this->seo()->setTitle(trans("meta.home.title"));
        $this->seo()->setDescription(trans("meta.home.description"));
        $this->seo()->metatags()->setKeywords(trans("meta.home.keywords"));
    }

    public function letterMeta()
    {
        $params = [
            'letter' => config('games.letters.' . array_search($this->route->parameters['letter'], config('games.letters'))),
        ];

        $this->seo()->setTitle(trans("meta.letter.title", $params));
        $this->seo()->setDescription(trans("meta.letter.description", $params));
        $this->seo()->metatags()->setKeywords(trans("meta.letter.keywords"));
    }
}
