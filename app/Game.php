<?php

namespace App;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Database\Eloquent\Model;
//use Terranet\Presentable\PresentableInterface;
use Terranet\Presentable\PresentableTrait;
use Terranet\Translatable\HasTranslations;
use Terranet\Translatable\Translatable;

class Game extends Model implements Translatable, StaplerableInterface//, PresentableInterface
{
    use HasTranslations, EloquentTrait {
        HasTranslations::getAttribute as getTranslatedAttribute;
        HasTranslations::setAttribute as setTranslatedAttribute;
        EloquentTrait::getAttribute as getStaplerableAttribute;
        EloquentTrait::setAttribute as setStaplerableAttribute;
    }
    //use PresentableTrait;
    protected static $unguarded = true;

    protected $fillable = [
        'url',
        'letter',
        'top',
        'views',
        'rank',
        'active',
        'desc',
        'image'
    ];

    protected $translatedAttributes = [
        'name',
        'description',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('image', [
            'styles' => $this->getStylesAttached()
        ]);

        parent::__construct($attributes);
    }

    public function getStylesAttached($hash = '#')
    {
        return [
            'medium' => '100x100' . $hash,
            'thumb' => '18x18' . $hash,
        ];
    }

    public function url()
    {
        return route('game.view', $this->slug);
    }

    public function slugs()
    {
        return $this->translations()->pluck('slug', 'language_id');
    }

    public function getAttribute($key)
    {
        if ($this->isKeyReturningTranslationText($key)) {
            if (!$translate = $this->getTranslatedAttribute($key)) {
                if ($translate = $this->translate(\localizer\getDefault()->id())) {
                    return $translate->$key;
                }
                return null;
            }
            return $translate;
        } else {
            if (array_key_exists($key, $this->attachedFiles)) {
                return $this->getStaplerableAttribute($key);
            }
        }
        return parent::getAttribute($key);
    }

    public function setAttribute($key, $value)
    {
        if ($this->hasTranslatedAttributes() && in_array($key, $this->translatedAttributes)) {
            return $this->setTranslatedAttribute($key, $value);
        } else {
            if (array_key_exists($key, $this->attachedFiles)) {
                return $this->setStaplerableAttribute($key, $value);
            }
        }
        return parent::setAttribute($key, $value);
    }

}
