<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class GameTranslation extends Model
{
    use Sluggable;

    public $timestamps = false;
    protected $fillable = [
        'name',
        'description',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'meta_title',
                'onUpdate' => false
            ]
        ];
    }

}
