<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameAdditionalTranslation extends Model
{
    public $timestamps = false;
}
