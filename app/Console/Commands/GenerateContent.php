<?php

namespace App\Console\Commands;

use App\Game;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class GenerateContent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:content {start} {end}';

    /**
     * The console command description.1
     *
     * @var string
     */
    protected $description = 'Generate content in new database.';

    protected $langs = [
        1 => 1,
        3 => 3,
        4 => 4,
        5 => 2,
    ];

    protected $locales;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        foreach (\localizer\locales() as $locale) {
            $this->locales[$locale->id()] = $locale;
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = (int)$this->argument('start');
        $end = (int)$this->argument('end');

        $letters = [];
        foreach (config('games.letters') as $key => $value) {
            $letters[$value] = $key;
        }

        list($items, $trans) = $this->getOldItems($start, $end);
        printf($items->count() . 'items.');
        $count = 0;
        foreach ($items as $item) {
            $count++;
            printf($count . ',');

            $data = $this->parseTrans($trans[$item->id]);
            $data['active'] = 1;
            $data['url'] = $item->car_url;
            $data['letter'] = (int) $letters[$item->car_letter];
            $data['created_at'] = $item->car_date;
            $data['views'] = $item->car_views;

            $game = new Game();
            $game->fill($data);
            try{
                $game->save();
            }catch (\Exception $e) {
                printf($e->getMessage());
            }

            if ($item->car_image) {
                $path = public_path('/data/cars/orig/' . $item->car_image);
                if(file_exists($path) && filesize($path)) {
                    $game->image = $path;
                    try{
                        $game->save();
                    }catch (\Exception $e) {
                        printf('---------------------');
                        $path = public_path('/data/cars/100X100/' . $item->car_image);

                        if(file_exists($path) && filesize($path)) {
                            $game->image = $path;
                            try{
                                $game->save();
                            }catch (\Exception $e) {
                                printf($e->getMessage());
                            }
                        }
                    }
                }else{
                    $path = public_path('/data/cars/100X100/' . $item->car_image);

                    if(file_exists($path) && filesize($path)) {
                        $game->image = $path;
                        try{
                            $game->save();
                        }catch (\Exception $e) {
                            printf($e->getMessage());
                        }
                    }
                }
            }
        }
    }

    private function parseTrans($items)
    {
        $results = [];
        foreach ($items as $lang => $item) {
            $results[$this->langs[$lang]] = [
                'name' => $item->cart_name,
                'description' => $item->cart_description,
                'meta_title' => trans('all.meta_title', ['name' => $item->cart_name],
                    $this->locales[$this->langs[$lang]]->iso6391()),
                'meta_description' => $this->setStringMeta($item->cart_name, $item->cart_description,
                    $this->locales[$this->langs[$lang]]->iso6391()),
                'meta_keywords' => $item->cart_tags . ', ' . trans('all.keywords', [],
                        $this->locales[$this->langs[$lang]]->iso6391()),
                'slug' => $item->cart_url,
            ];
        }

        return $results;
    }

    private function setStringMeta($name, $desc, $lang)
    {
        $name = trans('all.meta_title', ['name' => $name], $lang);

        $desc = $name . '. ' . (!$desc ? trans('all.description_games2', [], $lang) : $desc);

        $desc = strip_tags($desc);
        $order = array("\r\n", "\n", "\r", '"', '	', '  ');
        $desc = str_replace($order, '', $desc);
        $desc = str_limit($desc, 200);

        return $desc;
    }

    public function getOldItems($start, $end)
    {
        $games = DB::table('games_cars')->where('car_published', 1)->skip($start)->take($end)->get();

        $ids = [];

        foreach ($games as $game) {
            $ids[] = $game->id;
        }

        $items = DB::table('games_cars_trans')->whereIn('cart_car_id', $ids)->get();

        $gamesTrans = [];
        foreach ($items as $item) {
            $gamesTrans[$item->cart_car_id][$item->cart_language_id] = $item;
        }

        return [$games, $gamesTrans];
    }
}
