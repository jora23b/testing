<?php

namespace App\Http\Terranet\Administrator\Modules;

use Illuminate\Database\Eloquent\Model;
use Terranet\Administrator\Contracts\Module\Editable;
use Terranet\Administrator\Contracts\Module\Exportable;
use Terranet\Administrator\Contracts\Module\Filtrable;
use Terranet\Administrator\Contracts\Module\Navigable;
use Terranet\Administrator\Contracts\Module\Sortable;
use Terranet\Administrator\Contracts\Module\Validable;
use Terranet\Administrator\Form\Type\Select;
use Terranet\Administrator\Scaffolding;
use Terranet\Administrator\Traits\Module\AllowFormats;
use Terranet\Administrator\Traits\Module\AllowsNavigation;
use Terranet\Administrator\Traits\Module\HasFilters;
use Terranet\Administrator\Traits\Module\HasForm;
use Terranet\Administrator\Traits\Module\HasSortable;
use Terranet\Administrator\Traits\Module\ValidatesForm;

/**
 * Administrator Resource Games
 *
 * @package Terranet\Administrator
 */
class Games extends Scaffolding implements Navigable, Filtrable, Editable, Validable, Sortable, Exportable
{
    use HasFilters, HasForm, HasSortable, ValidatesForm, AllowFormats, AllowsNavigation;

    /**
     * The module Eloquent model
     *
     * @var string
     */
    protected $model = '\App\Game';

    public function columns()
    {
        $columns = $this->scaffoldColumns();
        $columns->without(['url', 'desc', 'description', 'slug', 'meta_title', 'meta_description', 'meta_keywords']);
        $columns->move('name', 'after:id');

        return $columns;
    }

    public function form()
    {
        $form = $this->scaffoldForm();
        $form->without(['desc', 'views', 'url']);

        $form->update('description', function ($element) {
            $element->setInput('tinymce')->setTranslatable(true);
        });

        $form->update('meta_description', function ($element) {
            $element->setInput('textarea')->setTranslatable(true);
        });

        $form->update('meta_keywords', function ($element) {
            $element->setInput('textarea')->setTranslatable(true);
        });

        $form->update('letter', function ($element) {
            $element->setInput(new Select('letter'));
            $element->getInput()->setOptions(config('games.letters'));

            return $element;
        });

        if($eloquent = app('scaffold.model')) {
            $eloquent->hasAttachedFile('image', [
                'styles' => $eloquent->getStylesAttached('')
            ]);
        }

        return $form;
    }

    public function rules()
    {
        return array_merge($this->scaffoldRules(), [
            'url' => '',
            'slug' => '',
            'rank' => '',
            'views' => '',
            'desc' => '',
            'letter' => '',
        ]);

    }
}