<?php

namespace App\Http\Controllers;

use App\Services\GameService;
use Illuminate\Http\Request;

class RssController extends Controller
{
    protected $service;

    public function __construct(GameService $service)
    {
        $this->service = $service;
    }

    public function index($letter)
    {
        $letter = array_search($letter, config('games.letters'));

        $games = $this->service->getByLetter($letter);

        return response(view('rss', compact('games', 'letter')))->header('Content-Type', 'application/xml');
    }
}
