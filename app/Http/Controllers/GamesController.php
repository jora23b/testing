<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 10/26/17
 * Time: 16:15
 */

namespace App\Http\Controllers;


use App\Game;
use App\Services\GameService;
use Illuminate\Http\Request;

class GamesController extends Controller
{
    protected $service;

    public function __construct(GameService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request, $letter = null)
    {
        $letter = $letter ? array_search($letter, config('games.letters')) : $letter;
        $q = $request->get('q');
        if ($q) {
            $filters['search'] = $q;
        } else {
            $filters['letter'] = $letter ? $letter : 0;
        }

        $games = $this->service->getPaginate($filters);

        $addUrl = [];
        if ($letter) {
            foreach (\localizer\locale() as $locale) {
                $addUrl[$locale->id()] = '/games/' . config('games.letters.' . $letter);
            }
        }
        $letter = (int) $letter;

        return view('games.index', compact('games', 'letter', 'addUrl', 'q'));
    }

    public function view(Game $game)
    {
        $addUrl = $game->slugs();

        foreach ($addUrl as $key => $value) {
            $addUrl[$key] = '/game/' . $value;
        }

        return view('games.view', compact('game', 'addUrl'));
    }
}