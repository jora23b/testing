<?php

namespace App\Http\Controllers;

use App\Services\GameService;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
    protected $service;

    public function __construct(GameService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        $games = $this->service->getAllForSitemap();

        return response(view('sitemap', compact('games')))->header('Content-Type', 'application/xml');
    }

    public function images()
    {
        $games = $this->service->getAllSitemapImages();

        return response(view('sitemap-images', compact('games')))->header('Content-Type', 'application/xml');
    }
}
