<?php

namespace App\Presenters;

use Terranet\Presentable\Presenter;

class GameAddiotionalPresenter extends Presenter
{
    public function adminGameId()
    {
        return with($game = $this->presentable->game) ? $game->name . ' (' . $this->game_id . ')' : null;
    }

}