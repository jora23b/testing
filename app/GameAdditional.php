<?php

namespace App;

use App\Presenters\GameAddiotionalPresenter;
use Illuminate\Database\Eloquent\Model;
use Terranet\Presentable\PresentableInterface;
use Terranet\Presentable\PresentableTrait;
use Terranet\Translatable\HasTranslations;
use Terranet\Translatable\Translatable;

class GameAdditional extends Model implements Translatable, PresentableInterface
{
    use HasTranslations, PresentableTrait;
    protected $presenter = GameAddiotionalPresenter::class;

    protected $fillable = [
        'game_id',
        'name',
        'email',
        'active',
    ];

    protected $translatedAttributes = ['description'];

    public function game()
    {
        return $this->belongsTo('App\Game');
    }
}
