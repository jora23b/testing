<?php

return [
    'home' => [
        'title' => 'Codici per giochi',
        'description' => 'Codici, Trainer, trucchi, parole, rimorchi per tutti i giochi in un solo sito. Tutti per giochi (aggiungere vita / armi / trasferimento di denaro ad un altro livello).',
        'keywords' => 'codici per giochi, codici, trucchi, parole, formatore, giochi',
    ],
    'letter' => [
        'title' => 'Codici per giochi con lettera :letter.',
        'description' => 'Codici, trainer, trucchi, parole, rimorchi per tutti giochi con lettera :letter. Tutti per giochi (aggiungere vita / armi / trasferimento di denaro ad un altro livello).',
        'keywords' => 'codici per giochi, codici, trucchi, parole, formatore, giochi',
    ],
    '404' => [
        'title' => 'Pagina non trovata',
        'description' => '404 Pagina non trovata',
        'keywords' => '',
    ],
];
