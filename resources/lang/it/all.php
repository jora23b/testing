<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'site' => 'http://codici-giochi.it',
    'meta_title' => 'Codici per :name',

	'msg_qe' => 'If you did not find what you are looking for, let us know about it by filling out the form below:',
	'email2_cc' => 'Optional',
	'share_for_particip' => 'To participate in competition, please share.',
	'send_success' => 'Your message has been sent successfully!',
	'send_message' => 'Send Message',
	'your_name' => 'Name',
	'subject' => 'Subject',
	'comment' => 'Comment',
	'message' => 'Message',
	'captcha' => 'Captcha',
	'email' => 'E-mail',
	'error_empty' => 'Not all fields completed!',
	'error_email' => 'Field "E-mail" is filled correctly',
	'error_exist_email' => 'This E-mail is already registered',
	'succes_send' => "Your message has been sent successfully",
	'error_captcha' => 'You have entered an invalid value for the captcha',
	'success_register' => 'You have successfully registered!',
	'title_share' => 'Win PlayStation 4.',
	'desc_share' => 'Participate in contest and win PlayStation 4...',
	
	'title_like' => 'Look what will happen if you put the class!',
	'desc_like' => 'It is impossible, just class.',
	
	'no_codes' => 'Per questo gioco abbiamo trovato nessun codice',
	'views' => 'Visualizzazioni',
	'about_us1' => 'Codici per giochi',
	'title_games' => 'Codici per giochi',
	'popular_posts' => 'Giochi populare',
	'codes' => 'Codici',
	'home' => 'Principale',
	'menu' => 'Menu',
	'search' => 'Ricerca',
	'result_search' => 'Risultati della ricerca',
	'details' => 'Dettagliato',
	'pages' => 'Pagine',
	
	
	'about_us' => 'Codici, Trainer, trucchi, le parole, rimorchi per tutti i giochi in un solo sito. Tutti per giochi (aggiungere vita / armi / trasferimento di denaro ad un altro livello).',
	'description_games' => 'Codici, trainer, trucchi, parole, rimorchi per tutti giochi con lettera',
	'description_games2' => 'Tutti per giochi (aggiungere vita / armi / trasferimento di denaro ad un altro livello).',
	'key_games' => 'codici per giochi, codici, trucchi, parole, formatore, giochi',
	'keywords' => 'codici, trucchi, parole, trainer, giochi',
	'google' => "<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-96565011-1', 'auto');
	  ga('send', 'pageview');

	</script>"
];
