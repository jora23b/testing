<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'site' => 'http://kod-igra.ru',
    'meta_title' => 'Коды для :name',

	'msg_qe' => 'Если не нашли то, что вы искали, дайте нам знать об этом, заполнив форму ниже:',
	'email2_cc' => 'Необязательно',
	'share_for_particip' => 'Share пожалуйста, для участия в конкурсе.',
	'send_success' => 'Ваше письмо успешно отправлено',
	'no_codes' => 'Для данной игры у нас нет чит кодов', 
	'views' => 'Прасмотров',
	'about_us1' => 'Чит-коды для игр',
	'about_us' => 'Коды, тренеры, читы, пароли для всех игр только на одном сайте. Все об обмане в играх (добавление жизней / оружия / денег, переход на нужный уровень).',
	'particip' => 'Участвовать',
	'email' => 'E-mail',
	
	'popular_posts' => 'Популярные игры',
	'codes' => 'Коды',
	'home' => 'Главная',
	'menu' => 'Меню',
	'search' => 'Поиск',
	'title_like' => 'Посмотри что будет если поставить класс!',
	'desc_like' => 'Это невозможно, всего лишь класс.',
	'title_share' => 'Выиграй PlayStation 4.',
	'desc_share' => 'Участвуй в конкурсе и выиграй абсолютно бесплатный PlayStation 4...',
	
	'country' => 'Страна',
	'game' => 'Игра',
	'error_exist_email' => 'Этот E-mail уже зарегистрирован',
	'success_particip' => 'Ваши данные были успешно зарегистрированы. Для участия в конкурсе необходимо нажать одну из кнопок ',
	'ms' => 'Va multumim pentru inregistrare!',
	'success_register' => 'Вы успешно зарегистрированы!',
	
	'title_games' => 'Чит-коды для игр',
	'description_games' => 'Коды, тренеры, читы, пароли для всех игр на букву',
	'description_games2' => 'Все об обмане в играх (добавление жизней / оружия / денег, переход на нужный уровень).',
	'key_games' => 'коды к играм, читы, код, игра, тренеры',
	
	'keywords' => 'коды, читы, пароли, трейнеры, прохождение игры',
	'template_name' => 'Имя шаблона',
	'downloads' => 'Скачяли',
	'software_required' => 'Необходимое программное обеспечение',
	'product_description' => 'Описание продукта',
	'width' => 'Ширина',
	'free_download' => 'Скачать',
	'category' => 'Раздел',
	'tags' => 'Тэги',
	'Todays_templates' => "Сегодняшний Шаблоны",
	'error_empty' => 'Не все обязательные поля заполнены',
	'error_email' => 'Поле "E-mail" заполнено некорректно',
	'succes_send' => "Ваше письмо успешно отправлено",
	'error_captcha' => 'Вы ввели неверное значение captcha',
	
	'your_name' => 'Ваше имя',
	'subject' => 'Тема',
	'comment' => 'Комент',
	'message' => 'Сообщение',
	'captcha' => 'Captcha',
	
	'post_comment' => 'Коментировать',
	'send_message' => 'Отправить',
	
	'comments' => 'Комментарии',
	'add_comment' => 'Добавить комментарии',
	'google' => "<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-96560497-1', 'auto');
	  ga('send', 'pageview');

	</script>"
];
