<?php

return [
    'home' => [
        'title' => 'Coduri pentru jocuri',
        'description' => 'Coduri, trainer, cheat-uri, parole, trailere pentru toate jocurile pe doar un singur site. Totul pentru jocuri (adauga viata / arme / bani, trecere la alt nivel).',
        'keywords' => 'coduri pentru jocuri, coduri, cheat-uri, parole, trainer, jocuri',
    ],
    'letter' => [
        'title' => 'Coduri pentru jocuri cu litera :letter.',
        'description' => 'Coduri, trainer, cheat-uri, parole, trailere pentru toate jocurile cu litera :letter. Totul pentru jocuri (adauga viata / arme / bani, trecere la alt nivel).',
        'keywords' => 'coduri pentru jocuri, coduri, cheat-uri, parole, trainer, jocuri',
    ],
    '404' => [
        'title' => 'Pagina nu există',
        'description' => '404 Pagina nu există',
        'keywords' => '',
    ],
];
