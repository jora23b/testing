<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'site' => 'http://coduri-jocuri.dev',
    'meta_title' => 'Coduri pentru :name',

	'msg_qe' => 'Dacă nu ați găsit ce căutați anunțați-ne despre asta completînd formularul de mai jos:',
	'email2_cc' => 'Obțional. Doar pentru a vă răspunde/anunța',
	'send_success' => 'Mesajul a fost trmis cu succes!',
	'send_message' => 'Trimite mesaj',
	'your_name' => 'Nume',
	'subject' => 'Subiect',
	'captcha' => 'Captcha',
	'email' => 'Email',
	'title_like' => 'Vezi ce va fi daca vei da un like!',
	'desc_like' => 'Asta este incredibil, doar un singur like.',
	'share_for_particip' => 'Un share va rog pentru a participa la concurs.',
	'particip' => 'Participa',
	'success_register' => 'Ati fost inregistrat cu success!',
	'title_share' => 'Cistiga PlayStation 4.',
	'desc_share' => 'Participa la concurs si cistiga un PlayStation 4...',
	
	'country' => 'Tara',
	'game' => 'Joc',
	'error_captcha' => 'Captcha incorect',
	'error_email' => 'E-mail incorect',
	'error_exist_email' => 'Acest E-mail deja este inregistrat',
	'success_particip' => 'Datele Dvs. au fost inregistrate cu succes. Pentru a participa la concurs a mai ramas sa da-ti un share!',
	'ms' => 'Va multumim pentru inregistrare!',
	
	'no_codes' => 'Pentru acest joc nu au fost găsite coduri',
	'views' => 'Vizualizări',
	'about_us1' => 'Coduri pentru jocuri',
	'about_us' => 'Coduri, trainer, cheat-uri, parole, trailere pentru toate jocurile pe doar un singur site. Totul pentru jocuri (adauga viata / arme / bani, trecere la alt nivel).',
	'popular_posts' => 'Jocuri populare',
	'codes' => 'Coduri',
	'home' => 'Principală',
	'menu' => 'Meniu',
	'title_games' => 'Coduri pentru jocuri',
	'description_games' => 'Coduri, trainer, cheat-uri, parole, trailere pentru toate jocurile cu litera',
	'description_games2' => 'Totul pentru jocuri (adauga viata / arme / bani, trecere la alt nivel).',
	'key_games' => 'coduri pentru jocuri, coduri, cheat-uri, parole, trainer, jocuri',
	
	'keywords' => 'coduri, cheat-uri, parole, trainer, jocuri',
	'search' => 'Căutare',
	'search_site' => 'Căutare pe site',
	'result_search' => 'Rezultatele căutării',
	'planshets' => 'Tablete',
	'laptops' => 'Laptopuri',
	'phones' => 'Telefoane',
	'keyboards' => 'keyboards',
	'details' => 'Detaliat',
	'catalog' => 'Catalog',
	'add_to_shopping' => 'în coș',
	'pages' => 'Pagini',
	'categories' => 'Categorii',
	'shopping' => 'Coș',
	'in_shopping' => 'în coș',
	'products1' => 'produse',
	'count' => 'Cantitate',
	'price' => 'Preț',
	'set_form' => 'Finalizează comanda',
	'all_ammount' => 'Total',
	'add_to_shopping2' => 'Adaugă în coș',
	'delete_for_shopping' => 'Elimină din coș',
	'close' => 'Închide',
	'name' => 'Nume',
	'phone' => 'Telefon',
	'edit' => 'Editează',
	'confirmation' => 'Confirmare comandă',
	'informations' => 'Informații',
	'comment' => 'Comentariu',
	'continue' => 'Continue',
	'products' => 'Produse',
	'refrash' => 'Actualizează',
	'delete' => 'Elimină',
	'shopping_empty' => 'Coșul este gol!',
	'recommandations' => 'Recomandări',
	'print_recommandation' => 'Lasă recomandare',
	'message' => 'Mesaj',
	'ansvers' => 'Întrebări și răspunsuri',
	'print_ansvers' => 'Lasă Întrebare',
	
	
	'error_empty' => 'Nu ați completat toate cîmpurile obligatorii!',
	
	'copyright' => '© 2013 TehnoLIFE.MD Toate drepturile rezervate.',
	
	'google' => "<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-50670477-1', 'auto');
	  ga('send', 'pageview');

	</script>"
];
