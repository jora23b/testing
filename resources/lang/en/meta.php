<?php

return [
    'home' => [
        'title' => 'Cheat / Solutions - Games',
        'description' => 'Codes, Trainer, cheats, passwords, trailers for all games on only one site. All Games (add life / weapons / money transfer to another level).',
        'keywords' => 'cheat, codes, level, passwords, hints',
    ],
    'letter' => [
        'title' => 'Cheat / Solutions - Games with letter :letter.',
        'description' => 'Codes, Trainer, cheats, passwords, trailers for all games with letter :letter. All Games (add life / weapons / money transfer to another level).',
        'keywords' => 'cheat, codes, level, passwords, hints',
    ],
    '404' => [
        'title' => 'Page not Found',
        'description' => '404 Page not Found',
        'keywords' => '',
    ],
];
