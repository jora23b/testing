<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'site' => 'http://coduri-jocuri.dev',
	'meta_title' => ':name cheats solutions',
	'msg_qe' => 'If you did not find what you are looking for, let us know about it by filling out the form below:',
	'email2_cc' => 'Optional',
	'email' => 'E-mail',
	'share_for_particip' => 'To participate in competition, please share.',
	'particip' => 'Participate',
	
	'send_success' => 'Your message has been sent successfully',
	'no_codes' => 'For this no cheat codes',
	'views' => 'Views',
	'about_us1' => 'Cheat / Solutions - Games',
	'about_us' => 'Codes, Trainer, cheats, passwords, trailers for all games on only one site. All Games (add life / weapons / money transfer to another level).',
	
	'popular_posts' => 'Popular posts',
	'codes' => 'Codes',
	'home' => 'Home',
	'menu' => 'Menu',
	'search' => 'Serach',
	
	'title_like' => 'Look what will happen if you put the class!',
	'desc_like' => 'It is impossible, just class.',
	
	'title_games' => 'Cheat / Solutions - Games',
	'description_games' => 'Codes, Trainer, cheats, passwords, trailers for all games with letter',
	'description_games2' => 'All Games (add life / weapons / money transfer to another level).',
	'key_games' => 'cheat, codes, level, passwords, hints',
	
	'keywords' => 'cheat, codes, level, passwords, hints',
	'template_name' => 'Template Name',
	'downloads' => 'Downloads',
	'software_required' => 'Software Required',
	'product_description' => 'Product Description',
	'width' => 'Width',
	'free_download' => 'Free download',
	'category' => 'Category',
	'tags' => 'Tags',
	'Todays_templates' => "Today's Templates",
	'error_empty' => 'Not all fields completed',
	'error_email' => 'Field "E-mail" is filled correctly',
	'error_exist_email' => 'This E-mail is already registered',
	'succes_send' => "Your message has been sent successfully",
	'error_captcha' => 'You have entered an invalid value for the captcha',
	'success_register' => 'You have successfully registered!',
	'title_share' => 'Win PlayStation 4.',
	'desc_share' => 'Participate in contest and win PlayStation 4...',
	
	'your_name' => 'Your Name',
	'subject' => 'Subject',
	'comment' => 'Comment',
	'message' => 'Message',
	'captcha' => 'Captcha',
	
	'post_comment' => 'Post Comment',
	'send_message' => 'Send Message',
	
	'comments' => 'Comments',
	'add_comment' => 'Add Comment',
	'google' => "<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-50670477-1', 'auto');
	  ga('send', 'pageview');

	</script>"

];
