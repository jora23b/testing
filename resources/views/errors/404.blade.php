@extends('layouts.app')

@section('content')
    @include('filter')
    @include('ads.ads1')
<div class="clear"></div>
    <div align="center" style="font-size: 60px;line-height: 60px; margin-top: 50px; font-weight: bold;">404</div>
    <div align="center" style="margin-top: 10px; margin-bottom: 50px; font-size: 16px; line-height: 16px;">{{ trans('meta.404.title') }}</div>
<div class="clear"></div>
@endsection
