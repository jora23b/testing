<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
        <channel>
                <title>{{ $letter == 0 ? trans('meta.home.title') : trans('meta.letter.title', ['letter' => config('games.letters.' . $letter)]) }}</title>
                <link>{{ route('home-page') }}</link>
                <description>{{ $letter == 0 ? trans('meta.home.description') : trans('meta.letter.description', ['letter' => config('games.letters.' . $letter)]) }}</description>
                <language>{{ \localizer\locale()->iso6391() }}</language>
                <image>
                        <title>{{ $letter == 0 ? trans('meta.home.title') : trans('meta.letter.title', ['letter' => config('games.letters.' . $letter)]) }}</title>
                        <url>{{ asset('/gfx/logo.jpg') }}</url>
                        <link>{{ route('home-page') }}</link>
                </image>
                <atom:link href="{{ route('rss', config('games.letters.' . $letter)) }}" rel="self" type="application/rss+xml" />
                @if(isset($games))
                        @foreach ($games as $game)
                                <item>
                                        <title><![CDATA[{{ $game->meta_title }}]]></title>
                                        <link><![CDATA[{{ $game->url() }}]]></link>
                                        <description><![CDATA[{{ $game->meta_description }}]]></description>
                                        <guid><![CDATA[{{ $game->url() }}]]></guid>
                                </item>
                        @endforeach
                @endif
        </channel>
</rss>
