<div class="clear"></div>
<div class="letters">
    @foreach(config('games.letters') as $key => $value)
        <a href="{{ $key ? route('game.letter', $value) : route('home-page') }}" class="{{ isset($letter) && $key == $letter ? 'active' : '' }}">{{ $value }}</a>
    @endforeach
</div>