<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($games as $game)
        <url><loc>{{ asset($game->image->url('original')) }}</loc></url>
    @endforeach
</urlset>
