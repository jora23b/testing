@extends('layouts.app')
@section('content')

    @include('filter')
    <div class="game" itemscope itemtype="http://schema.org/Game">
        <meta itemprop="inLanguage" content="{{ App::getLocale()}}">
        <div>
            <h1 itemprop="name" style="text-transform: none;font-size:20px;">{{$game->meta_title}}</h1>
        </div>
        @include('ads.ads_game_top')

        <div style="margin-top:10px;color:#494949;">
            @if($game->image->originalFilename())
                <a style="margin-left:10px;margin-bottom:10px;margin-right:10px;float:left;" class="fancybox" rel="as"
                   href="{{ $game->image->url() }}">
                    <img style="max-width:100px;max-height:100px;" itemprop="image"
                         src="{{ $game->image->url('medium') }}" alt="{{$game->name}}"/>
                </a>
            @endif

            <p itemprop="description">
                @if($game->description)
                    {!! $game->description !!}
                @else
                    <span rel="nofollow">{{trans('all.no_codes')}}</span>
                @endif
            </p>
        </div>
        @include('ads.ads_game_footer')
    </div>

@endsection