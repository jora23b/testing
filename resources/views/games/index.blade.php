@extends('layouts.app')

@section('content')
    @include('filter')
    @include('ads.ads1')
<div class="clear"></div>
@if(isset($games))
    <div class="items">
        @foreach ($games as $index => $game)
            <div class="item" itemscope itemtype="http://schema.org/Game">
                <a itemprop="url" href="{{ $game->url() }}">
                    @if($game->image->originalFilename())
                        <img alt="{{ $game->name }}" src="{{ $game->image->url('medium') }}"/>
                    @endif
                    <span itemprop="name">{{ $game->name }}</span>
                </a>
            </div>
            @if($index == 9)
                @include('ads.ads2')
            @elseif($index == 19)
                @include('ads.ads3')
            @endif
        @endforeach
    </div>
    <div class="pagination" align="center">
        {{ $games->links() }}
    </div>
@endif
<div class="clear"></div>
@endsection
