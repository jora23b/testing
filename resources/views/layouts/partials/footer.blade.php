<div id="fb-root"></div>
<footer>
    <div class="info">
        <strong>{{trans('all.about_us1')}}</strong><br>
        {{trans('all.about_us')}}
    </div>
    <div class="copyright">
        <span>{{ Config::get('app.site_company') }} © 2014 - {{ @date('Y') }}</span>
        <div class="menu_footer">
            <a target="_blanck" href="{{ route('rss', isset($letter) && $letter ? config('games.letters.' . $letter ) : config('games.letters.0')) }}">Rss</a>
        </div>
    </div>
    <div class="clear"></div>
    <div id="facebook-container"></div>
</footer>
<div class="clear"></div>
{!! trans('all.google') !!}
