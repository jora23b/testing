<header>
    <div class="languages">
        @foreach(\localizer\locales() as $locale)
            @if($locale->id() == \localizer\locale()->id())
                <span>{{ $locale->iso6391() }}</span>
            @else
                <a href="{{ trans('all.site', [], $locale->iso6391()) }}{{ !$locale->isDefault() ? '/' . $locale->iso6391() : '' }}{{ isset($addUrl[$locale->id()]) ? $addUrl[$locale->id()] : '' }}">
                    {{ $locale->iso6391() }}</a>
            @endif
        @endforeach
    </div>
    <div class="menu_container">
        <div class="logo" itemscope itemtype="http://schema.org/Organization">
            <a itemprop="url" href="{{ route('home-page') }}">
                <img itemprop="logo" src="{{ asset('/gfx/logo.jpg') }}">
            </a>
            <span itemprop="name" style="display:none">{{Config::get('app.site_company')}}</span>
        </div>
    </div>
    <div class="search">
        <form id="search" action="{{ route('home-page') }}" method="GET">
            <input type="text" onfocus="if(this.value =='{{trans('all.search')}}') this.value=''" onblur="if(this.value=='') this.value='{{ trans('all.search') }}'" value="{{ isset($q) && $q ? $q : trans('all.search') }}" name="q" />
            <div class="clear"></div>
            <a onclick="document.getElementById(&#39;search&#39;).submit()" class="search-marker"></a>
        </form>
    </div>
    <div class="clear"></div>
</header>