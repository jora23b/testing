<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0//{{ config('app.locale') }}" "http://www.w3.org/TR/xhtml1/DTD/xhtml11.dtd">
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        @include('layouts.partials.seo')
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">


        <link rel="icon" href="/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

        <META HTTP-EQUIV="CONTENT-LANGUAGE" CONTENT="{{ config('app.locale') }}">

        <meta name="msvalidate.01" content="8D6084A9335D89EF8D7ED2ABD0D9FEBB" />
        <meta name="telderi" content="176be42ed207daf2ed8937fee9db7551" />
        @if(isset($languages))
            @foreach($languages as $lang)
                <link rel="alternate" href="http://{{$lang['domain']}}{{$lang['prefix']}}{{$lang['url']}}" hreflang="{{$lang['name']}}" />
            @endforeach
        @endif
    </head>
    <body>
        <div class="main">
            <div class="chd_main">
                @include('layouts.partials.header')
                <div class="content">
                    @yield('content')
                </div>
                @include('layouts.partials.footer')
            </div>
        </div>
    <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
