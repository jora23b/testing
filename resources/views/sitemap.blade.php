<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <url><loc>{{ route('home-page') }}</loc></url>
    @foreach(config('games.letters') as $key => $value)
        <url><loc>{{ route('game.letter', $key) }}</loc></url>
    @endforeach
    @foreach ($games as $game)
        <url><loc>{{ $game->url() }}</loc></url>
    @endforeach
</urlset>
